#include "Vector.h"
#include <string>
#include <memory>

using namespace std;
Vector::Vector(int n)
	: _elements(new int[n]), _capacity( n ), _resizeFactor( n){}

Vector::~Vector()
{
	delete[] this->_elements;
	this->_elements = nullptr;
}

int Vector::size() const { return this->_size; }

int Vector::capacity() const { return this->_capacity; }

int Vector::resizeFactor() const {return this->_resizeFactor; }

bool Vector::empty() const {return this->_size==0? true : false; }

void Vector::push_back(const int& val) 
{
	if (this->_capacity == this->_size) 
	{
		this->_capacity += this->_resizeFactor;
		int* tempVec = new int[this->_capacity];
		for (int i = 0; i < this->_size; i++) 
		{
			tempVec[i] = this->_elements[i];
		}
		delete[] this->_elements;
		this->_elements = tempVec;
	}
	this->_elements[this->_size] = val;
	this->_size++;
}

int Vector::pop_back()
{
	if (this->_size == 0)
	{
		cout << "ERROR: pop from empty vector!";
		return ERROR_EMPTY_VECTOR;
	}
	int a = this->_elements[this->_size-1];
	this->_elements[this->_size] = 0;
	this->_size--;
	return a;
}

void Vector::reserve(int n)
{
	while (this->_capacity <= n)
		this->_capacity += this->_resizeFactor;
	int* newVec = new int[this->_capacity];
	for (int i = 0; i < this->_capacity; i++)
	{
		newVec[i] = this->_elements[i];
	}
	delete[] this->_elements;
	this->_elements = newVec;
}

void Vector::resize(int n)
{
	if (n < this->_size)
	{
		for (int i = this->_size; i < n; i++)
		{
			this->_elements[i] = 0;
		}
	}
	if (n > this->_size && n <= this->_capacity)
	{
		this->_size = n;
	}
	else
	{
		this->reserve(n);
	}
}

void Vector::assign(int val)
{
	for (int i = 0; i < this->_size; i++)
	{
		this->_elements[i] = val;
	}
}

void Vector::resize(int n, const int & val)
{
	if (n < this->_size)
	{
		for (int i = this->_size; i < n; i++)
		{
			this->_elements[i] = 0;
		}
	}
	if (n > this->_size && n <= this->_capacity)
	{
		for (int i = this->_size; i < n; i++)
		{
			this->_elements[i] = val;
		}
		this->_size = n;
	}
	else
	{
		int tempSize = this->_size;
		this->reserve(n);
		for (int i = tempSize; i < n; i++)
		{
			this->_elements[i] = val;
		}
	}
}

Vector::Vector(const Vector & other){ *this = other; }

Vector & Vector::operator=(const Vector & other)
{
	this->_capacity = other._capacity;
	this->_resizeFactor = other._resizeFactor;
	this->_size = other._size;
	this->_elements = new int[other.capacity()];
	for (int i = 0; i < other.size(); i++) 
	{
		this->_elements[i] = other[i];
	}
	return *this;
}

int & Vector::operator[](int n) const { return this->_elements[n]; }
/*
__
| |__    ___   _ __   _   _  ___
| '_ \  / _ \ | '_ \ | | | |/ __|
| |_) || (_) || | | || |_| |\__ \
|_.__/  \___/ |_| |_| \__,_||___/
*/

Vector & Vector::operator+(const Vector & other)
{
	Vector *ret = new Vector(this->_resizeFactor);
	ret->resize(this->_size > other.size() ? this->_size : other.size());

	for (int i = 0; i < ret->capacity(); i++)
	{
		ret->push_back( this->_elements[i] + other[i]);
	}
	return *ret;
}

Vector & Vector::operator-(const Vector & other)
{
	Vector *ret = new Vector(this->_resizeFactor);
	ret->resize(this->_size > other.size() ? this->_size : other.size());

	for (int i = 0; i < ret->capacity(); i++)
	{
		ret->push_back(this->_elements[i] - other[i]);
	}
	return *ret;
}

void Vector::operator-=(const Vector & other)
{
	for (int i = 0; i < this->capacity(); i++)
	{
		this->_elements[i] = (this->_elements[i] - other[i]);
	}
}

void Vector::operator+=(const Vector & other)
{
	for (int i = 0; i < this->capacity(); i++)
	{
		this->_elements[i] = (this->_elements[i] + other[i]);
	}
}

//best onliner ever! >_<
ostream & operator<<(ostream & stream, Vector & vector)
{
	return stream << "\nVector Info:\ncapacity is " << vector._capacity << "\nSize is " << vector._size << "\nData is {" << [&]() {shared_ptr<string> str = make_shared<string>(); for (int i = 0; i < vector._size - 1; i++) *str += to_string(vector._elements[i]) + ", "; *str += to_string(vector._elements[vector._size - 1]); return ((*str) + "}\n"); }();
}
//
//int main()
//{
//	Vector* v = new Vector(3);
//	Vector* v1 = new Vector(3);
//	cout << "\n----------------------V---------------------\n";
//	v->push_back(5);
//	v->push_back(5);
//	v->push_back(5);
//	v->push_back(5);
//	v->push_back(5);
//	v->assign(15);
//	for (size_t n = 0; n < v->size(); n++)
//		cout << (*v)[n] << ", ";
//	cout << endl;
//	cout << "\n----------------------V1---------------------\n";
//	v1->push_back(5);
//	v1->push_back(5);
//	v1->push_back(5);
//	v1->push_back(5);
//	v1->push_back(5);
//	v1->assign(5);
//	for (size_t n = 0; n < v1->size(); n++)
//		cout << (*v1)[n] << ", ";
//	cout << endl;
//	Vector* v2 = new Vector(0);
//	*v2 = *v - *v1;
//	for (size_t n = 0; n < v->size(); n++)
//		cout << (*v2)[n] << ", ";
//	cout << endl;
//	*v2 += *v;
//	for (size_t n = 0; n < v->size(); n++)
//		cout << (*v2)[n] << ", ";
//	cout << endl;
//	cout << *v;
//	system("pause");
//}

